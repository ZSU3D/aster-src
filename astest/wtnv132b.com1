# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2023 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# person_in_charge: marc.kham at edf.fr
#
#                    POST-TRAITEMENT ET VALIDATION
#                    =============================

# ATTENTION:
# =========
#
# on ne garantit pas la transposabilite de ce post-traitement
# a un autre maillage.


POURSUITE(CODE="OUI")

#       ---------------
# FIN-  Post-traitement
#       ---------------

#    ----------------------------------------------------
# 1- On cherche d'abord à créer une table de la forme:
#
#                    temps noeu_1  noeu_2   ....   noeu_n
#                t1
#                t2
#  noeu_depl =    .
#                 .
#                 .
#                tn
#    ----------------------------------------------------


n_pas_temps_max = 10000
n_pas_incr = 1
noeu_depl = dict()
noeu_depl["TEMPS"] = [0.0]
cle = []
list_pas_temps = []
noeu_tass_0 = []

# -->20: début boucle for
for n in range(nbcouche):

    cle.append("ELEMEN" + str(n))
    noeu_depl[cle[n]] = [0.0] * n_pas_temps_max

    tab = T_DEP[n].EXTR_TABLE()
    t = tab.INST.values()
    dy = tab.DY.values()

    n_pas_temps = int(len(t) / (n + 1))

    n_pas_incr_0 = n_pas_incr
    n_pas_incr += n_pas_temps

    list_pas_temps.append(n_pas_incr - 2)

    noeu_depl["TEMPS"] += t[:: n + 1]
    noeu_tass_0.append(dy[(n + 1) * n_pas_temps - 1])

    # -->30: début boucle for
    for m in range(n + 1):

        noeu_depl[cle[m]][n_pas_incr_0:n_pas_incr] = dy[m :: n + 1]

    # <--30: fin boucle for

    # DETRUIRE(NOM=(v))

# <--20: fin boucle for


# -->50: début boucle for
for n in range(nbcouche):

    del noeu_depl[cle[n]][n_pas_incr:]

# <--50: fin boucle for


#    -------------------------------------------------
# 2- On calcule le tassement
#
#                    temps_1  temps_2   ....   temps_n
#                H1
#                H2
#  noeu_tass =    .
#                 .
#                 .
#                Hn
#    -------------------------------------------------


n_pas_temps = len(noeu_depl["TEMPS"])
noeu_tass = dict()

# -->100: début boucle for
for n in range(n_pas_temps - 1):

    noeu_tass["TEMPS" + str(n)] = []

# <--100: fin boucle for


# -->300: début boucle for
for n in range(n_pas_temps - 1):

    # -->40: début boucle for
    for m in range(nbcouche):

        tass_value = 0.0
        if noeu_depl[cle[m]][n + 1] != 0.0:
            tass_value = noeu_depl[cle[m]][n + 1] - noeu_tass_0[m]

        noeu_tass["TEMPS" + str(n)].append(tass_value)

    # <--40: fin boucle for

# <--300: fin boucle for


#    ----------------------------
# 3- Impression au format XMGRACE
#    ----------------------------


tass = [0.0] * n_pas_temps
COURBE = []
n_pas_temps = len(list_pas_temps)

# -->200: début boucle for
for n in range(n_pas_temps):

    def_courbe = {"ABSCISSE": noeu_tass["TEMPS" + str(list_pas_temps[n])]}
    def_courbe["ORDONNEE"] = list(range(1, nbcouche + 1))
    def_courbe["LEGENDE"] = "TEMPS" + str(list_pas_temps[n])
    def_courbe["COULEUR"] = list_pas_temps[n] + 1
    def_courbe["MARQUEUR"] = list_pas_temps[n] + 1

    COURBE.append(def_courbe)

    l_cote = DEFI_LIST_REEL(TITRE="COTE", VALE=def_courbe["ORDONNEE"])

    l_tass = DEFI_LIST_REEL(TITRE="TASSEMENT", VALE=def_courbe["ABSCISSE"])

    tass[n] = DEFI_FONCTION(NOM_PARA="Z", NOM_RESU="TASSEMENT", VALE_PARA=l_cote, VALE_FONC=l_tass)

    IMPR_FONCTION(FORMAT="TABLEAU", UNITE=8, COURBE=_F(FONCTION=tass[n]))

    del def_courbe
    DETRUIRE(NOM=(l_cote, l_tass))

# <--200: fin boucle for

IMPR_FONCTION(FORMAT="XMGRACE", UNITE=25, COURBE=COURBE)


#              ------------------------------------
# VALIDATION-  Comparaison des valeurs de tassement
#              par rapport à GEFDYN (ECP)
#              ------------------------------------


# tassement de la première couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-4.332271020453e-03,
            VALE_REFE=-4.6480000000000002e-3,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.07,
            FONCTION=tass[2],
        ),
        _F(
            VALE_CALC=-8.103724679664e-03,
            VALE_REFE=-8.3909999999999992e-3,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.035,
            FONCTION=tass[3],
        ),
        _F(
            VALE_CALC=-0.01169511964288,
            VALE_REFE=-0.011939999999999999,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[4],
        ),
        _F(
            VALE_CALC=-0.01523987914003,
            VALE_REFE=-0.015440000000000001,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[5],
        ),
        _F(
            VALE_CALC=-0.01881828076246,
            VALE_REFE=-0.01898,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[6],
        ),
        _F(
            VALE_CALC=-0.02250190025923,
            VALE_REFE=-0.02265,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=1.0e-2,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.02632556632365,
            VALE_REFE=-0.02647,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=1.0e-2,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.03031063056950,
            VALE_REFE=-0.03049,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.03446830222155,
            VALE_REFE=-0.034729999999999997,
            CRITERE="RELATIF",
            VALE_PARA=2.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la deuxième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-8.104062216192e-03,
            VALE_REFE=-8.4089999999999998e-3,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.089999999999999997,
            FONCTION=tass[3],
        ),
        _F(
            VALE_CALC=-0.01546407414811,
            VALE_REFE=-0.015720000000000001,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.050000000000000003,
            FONCTION=tass[4],
        ),
        _F(
            VALE_CALC=-0.02259809066920,
            VALE_REFE=-0.022780000000000002,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.040000000000000001,
            FONCTION=tass[5],
        ),
        _F(
            VALE_CALC=-0.02971915922573,
            VALE_REFE=-0.029829999999999999,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.040000000000000001,
            FONCTION=tass[6],
        ),
        _F(
            VALE_CALC=-0.03697876716745,
            VALE_REFE=-0.03703,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.04448336569136,
            VALE_REFE=-0.044510000000000001,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.05228909354717,
            VALE_REFE=-0.052359999999999997,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.06042850273580,
            VALE_REFE=-0.060609999999999997,
            CRITERE="RELATIF",
            VALE_PARA=3.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la troixième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.01169251941885,
            VALE_REFE=-0.011979999999999999,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.12,
            FONCTION=tass[4],
        ),
        _F(
            VALE_CALC=-0.02259616588864,
            VALE_REFE=-0.022800000000000001,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.070000000000000007,
            FONCTION=tass[5],
        ),
        _F(
            VALE_CALC=-0.03330729250638,
            VALE_REFE=-0.033410000000000002,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.059999999999999998,
            FONCTION=tass[6],
        ),
        _F(
            VALE_CALC=-0.04411040848055,
            VALE_REFE=-0.044119999999999999,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.050000000000000003,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.05519200859910,
            VALE_REFE=-0.055149999999999998,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.040000000000000001,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.06667975503141,
            VALE_REFE=-0.066650000000000001,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.07864097494638,
            VALE_REFE=-0.078729999999999994,
            CRITERE="RELATIF",
            VALE_PARA=4.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la quatrième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.01523443882174,
            VALE_REFE=-0.015469999999999999,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.089999999999999997,
            FONCTION=tass[5],
        ),
        _F(
            VALE_CALC=-0.02971307751101,
            VALE_REFE=-0.029839999999999998,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.050000000000000003,
            FONCTION=tass[6],
        ),
        _F(
            VALE_CALC=-0.04410395918457,
            VALE_REFE=-0.044110000000000003,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.040000000000000001,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.05872641144188,
            VALE_REFE=-0.058650000000000001,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.07378792080324,
            VALE_REFE=-0.073690000000000005,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.08942736137702,
            VALE_REFE=-0.089429999999999996,
            CRITERE="RELATIF",
            VALE_PARA=5.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la cinquième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.01880843069726,
            VALE_REFE=-0.019019999999999999,
            CRITERE="RELATIF",
            VALE_PARA=6.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.059999999999999998,
            FONCTION=tass[6],
        ),
        _F(
            VALE_CALC=-0.03696608701863,
            VALE_REFE=-0.03705,
            CRITERE="RELATIF",
            VALE_PARA=6.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.040000000000000001,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.05517544339218,
            VALE_REFE=-0.055149999999999998,
            CRITERE="RELATIF",
            VALE_PARA=6.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.07377678653934,
            VALE_REFE=-0.073700000000000002,
            CRITERE="RELATIF",
            VALE_PARA=6.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.09298877298342,
            VALE_REFE=-0.092979999999999993,
            CRITERE="RELATIF",
            VALE_PARA=6.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=1.0e-2,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la sixième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.02248607838816,
            VALE_REFE=-0.022679999999999999,
            CRITERE="RELATIF",
            VALE_PARA=7.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.059999999999999998,
            FONCTION=tass[7],
        ),
        _F(
            VALE_CALC=-0.04446083155795,
            VALE_REFE=-0.044540000000000003,
            CRITERE="RELATIF",
            VALE_PARA=7.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.06664754981510,
            VALE_REFE=-0.066650000000000001,
            CRITERE="RELATIF",
            VALE_PARA=7.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.08939758613505,
            VALE_REFE=-0.089440000000000006,
            CRITERE="RELATIF",
            VALE_PARA=7.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la septième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.02630181488765,
            VALE_REFE=-0.026499999999999999,
            CRITERE="RELATIF",
            VALE_PARA=8.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.050000000000000003,
            FONCTION=tass[8],
        ),
        _F(
            VALE_CALC=-0.05225269527472,
            VALE_REFE=-0.052380000000000003,
            CRITERE="RELATIF",
            VALE_PARA=8.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.07858673324252,
            VALE_REFE=-0.078719999999999998,
            CRITERE="RELATIF",
            VALE_PARA=8.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la huitième couche
TEST_FONCTION(
    VALEUR=(
        _F(
            VALE_CALC=-0.03027648307390,
            VALE_REFE=-0.030519999999999999,
            CRITERE="RELATIF",
            VALE_PARA=9.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.029999999999999999,
            FONCTION=tass[9],
        ),
        _F(
            VALE_CALC=-0.06037331850375,
            VALE_REFE=-0.060630000000000003,
            CRITERE="RELATIF",
            VALE_PARA=9.0,
            REFERENCE="SOURCE_EXTERNE",
            PRECISION=0.02,
            FONCTION=tass[10],
        ),
    )
)

# tassement de la neuvième couche
TEST_FONCTION(
    VALEUR=_F(
        VALE_CALC=-0.03442091687931,
        VALE_REFE=-0.034750000000000003,
        CRITERE="RELATIF",
        VALE_PARA=10.0,
        REFERENCE="SOURCE_EXTERNE",
        PRECISION=0.025000000000000001,
        FONCTION=tass[10],
    )
)

FIN()
